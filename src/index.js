//import { render } from '@testing-library/react';
import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';



import App from './App';

// import reportWebVitals from './reportWebVitals';

ReactDOM.render(
  <Fragment>
    <App />
  </Fragment>,
  document.getElementById('root')
);
//ReactDOM.render(what to render, where to render);
