import {Fragment, useContext, useEffect, useState } from 'react'
import AdminView from './AdminView'
import CourseCard from './../components/CourseCard'
import UserContext from './../UserContext'
const admin = localStorage.getItem('admin')
const token = localStorage.getItem('token')

export default function Courses(){

	const { state, dispatch } = useContext(UserContext)
	console.log(state)

	const [courses, setCourses] = useState([])

	useEffect( () => {
		if(admin === "false"){
			fetch(`http://localhost:3007/api/courses/isActive`, {
				method: "GET",
				headers:{
					"Authorization": `Bearer ${token}`
				}
			})
			.then(response => response.json())
			.then(response => {

				if(token !== null){
					dispatch({type: "USER", payload: true})
				}
				
				setCourses(
					response.map(course => {
						// console.log(course)
						return <CourseCard key={course._id} courseProp={course}/>
					})
				)
			})
		}

	}, [])

	return(
		<Fragment>
			{
				admin === "false" ?
					<Fragment>
						{courses}
					</Fragment>
				:
					<Fragment>
						<AdminView />
					</Fragment>
			}
		</Fragment>
	)
}
