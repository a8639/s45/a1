import {useState, useEffect} from 'react'
import {Card, Row, Col, Button} from "react-bootstrap"
import { Link } from 'react-router-dom'

export default function CourseCard({courseProp}) {
	console.log(courseProp)

	const {courseName, description, price, _id} = courseProp
	
	return(
		<Card className="m-5">
		  <Card.Body>
		    <Card.Title>{courseName}</Card.Title>
		    <Card.Subtitle>Description:</Card.Subtitle>
		    <Card.Text>
		      {description}
		    </Card.Text>
		    <Card.Subtitle>Price:</Card.Subtitle>
		    <Card.Text>
		    	{price}
		    </Card.Text>
		    <Link to={`/courses/${_id}`}> Check Course </Link>
		  </Card.Body>
		</Card>
	)
}
