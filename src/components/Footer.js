export default function Footer() {
	return(
		<div className="bg-info text-white d-flex justify-content-center align-items-center fixed-bottom" style={{height: '10vh', marginTop: '50px'}}>
			<p className="m-0 font-weight-bold">Amiel Canta &#64; Course Booking | Zuitt Coding Bootcamp &#169;
			</p>
		</div>

	)
}
